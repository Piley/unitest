<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class MainController extends Controller
{
    public function index(Request $request)
    {
        $ip = $request->get('ip');
        $int = ip2long($ip);

        $data = Cache::remember($int, 30, function () use ($int) {
            return DB::table('net_city_ip')
                ->join('net_city', 'net_city_ip.city_id', '=', 'net_city.id')
                ->join('net_country', 'net_city.country_id', '=', 'net_country.id')
                ->select('net_country.name_en as country_name', 'net_city.name_en as city_name', 'net_city.latitude', 'net_city.longitude')
                ->where('net_city_ip.begin_ip', '<=', $int)
                ->where('net_city_ip.end_ip', '>=', $int)
                ->first();
        });

        if (!$data) {
            return response()->json(null, 404);
        }

        return response()->json($data);
    }
}
